﻿var SearchClient = function () {

    var domain = "http://localhost:50681/api/Pokemon/get";
    var hints = [];
    var $searchInput;
    var $autocompleteTmpl;
    var $contentTmpl;
    var $autocompleteWrapper;

    var init = function () {
        $searchInput = $("#searchPokemon");
        $autocompleteTmpl = $("#autocompleteTmpl");
        $autocompleteWrapper = $(".autocomplete");
        $contentTmpl = $("#contentTmpl");
        $contentWrapper = $(".main");
        getHints();
    };

    var getHints = function () {
        $.ajax(domain, {
            success: success,
            error: error
        });
    };

    var success = function (data) {
        hints = data;
        registerEventListeners();
    };

    var error = function () { alert("Nastąpił błąd pobierania listy nazw"); };

    var registerEventListeners = function () {
        $searchInput.on('keyup', inputChange);
        $autocompleteWrapper.on('click', '.autocomplete-item', hintClick);
    };

    var inputChange = function (e) {
        var key;
        if (window.event) { // IE                    
            key = e.keyCode;
        } else if (e.which) { // Netscape/Firefox/Opera                   
            key = e.which;
        }

        //skrócona wersja
        key = e.which || e.keyCode || 0;

        if (key === 13) {
            search();
        }
        else {
            var inputValue = $(this).val();
            var tab = hints.filter(function (x) { return x.toLowerCase().indexOf(inputValue.toLowerCase()) > -1 });
            $autocompleteWrapper.html($autocompleteTmpl.tmpl({ elements: tab }));
        }
    };

    var hintClick = function () {
        $searchInput.val($(this).html());
        $autocompleteWrapper.html("");
        $searchInput.focus();
    };

    var search = function () {
        var inputValue = $searchInput.val();

        $.ajax(domain + "?name=" + inputValue, {
            success: successSearch,
            error: errorSearch
        });
    };

    successSearch = function (data) {
        $contentWrapper.html($contentTmpl.tmpl(
            {
                name: data.TemplateId.split('POKEMON_')[1],
                def: data.Pokemon.Stats.BaseDefense,
                stamina: data.Pokemon.Stats.BaseStamina,
                atk: data.Pokemon.Stats.BaseAttack,
                number: data.TemplateId.slice(2,5),
                BaseCaptureRate: data.Pokemon.Encounter.BaseCaptureRate,
                BaseFleeRate: data.Pokemon.Encounter.BaseFleeRate,
                PokedexHeightM: data.Pokemon.PokedexHeightM,
                PokedexWeightKg: data.Pokemon.PokedexWeightKg,
                CandyToEvolve: data.Pokemon.CandyToEvolve
            }
            ));
    };

    errorSearch = function () { alert("Nastąpił błąd pobierania pokemona"); };

    return {
        init: init
    };
}();