﻿var NavBarClient = function () {

    var domain = "http://localhost:50681/api/authorize/get";
    var $navBarTmpl;
    var token;

    var init = function () {
        //wspomnij dlaczego nie this
        $navBarTmpl = $("#navBarTmpl");
        get();
    };

    var get = function () {
        //zapierdol czystego xhra żeby pokazać jak jest chujnia
        //-----------------------------------------------------
        //var xmlHttp = new XMLHttpRequest();
        //xmlHttp.onreadystatechange = function () {
        //    if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
        //        success(JSON.parse(xmlHttp.responseText));
        //};
        //xmlHttp.open("GET", domain, true); // true for asynchronous 
        //xmlHttp.send(null);


        //może pokaż zwykłego geta
        //------------------------
        //$.get(domain, success);


        $.ajax(domain, {
            success: success,
            error: error
        });
    };

    var success = function (data) {
        token = data.authKey;
        $navBarTmpl.tmpl({ avatar: data.avatar, name: data.name }).appendTo(".navbar");
    };

    var error = function () { alert("Nastąpił błąd pobierania paska nawigacyjnego"); };

    return {
        init: init
    };
}();